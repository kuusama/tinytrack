<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once 'classes/user.php';
include_once 'classes/tariff.php';
include_once 'classes/task.php';
include_once 'classes/entry.php';
include_once 'classes/report.php';
include_once 'classes/database.php';
include_once 'classes/company.php';

define ('SITE_ROOT', realpath(dirname(__FILE__)));

function reject() {
    header("HTTP/1.1 404 Not Found");
    exit();
}

function msg($success, $status, $message, $extra = []) {
    return array_merge([
        'success' => $success,
        'status' => $status,
        'message' => $message
    ],$extra);
}

function checkToken($conn, $level) {

    if (! preg_match('/Bearer\s(\S+)/', $_SERVER['HTTP_AUTHORIZATION'], $matches)) {
        return false;
    }

    $token = $matches[1];

    if (!$token) {
        return false;
    } else {
        $user = new User($conn);
        return $user->checkToken($token, $level);
    }
}

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );

$requestMethod = $_SERVER["REQUEST_METHOD"];
$data = json_decode(file_get_contents("php://input"));
$returnData = [];
$status = 400;

$db = new Database();
$conn = $db->getConnection();

switch ($uri[2]) {

    case "user":
        $user = new User($conn);
        switch($requestMethod) {
            //case 'PUT': //Update
                // if (!checkToken($conn, 1))  {
                //     $returnData = msg(0, 401, 'Unauthorized');
                // } else {
                //     $res = $user->update($data->id, $data->login, $data->email, $data->pwd, $data->level);

                //     if(false == $res) {
                //         $returnData = msg(0, 422, 'Не удалось сохранить пользователя', ['errors' => $user->errors]);
                //     } else {
                //         $status = 200;
                //         $returnData = $res;
                //     }
                // }
            //break;

            //case 'DELETE': //Delete
                // $userId = $uri[3];
                // $res = $user->delete($userId);
                // if(false == $res) {
                //     $errors['common'] = 'Не удалось удалить пользователя';
                //     $returnData = msg(0, 422, 'Не удалось удалить пользователя', ['errors' => $user->errors]);
                // } else {
                //     $status = 200;
                //     $returnData = $res;
                // }
            //break;

            case 'POST': //Login or create or list
                if ('login' == $uri[3]) {
                    $res = $user->login($data->login, $data->password);
                    if(false == $res) {
                        $returnData = msg(0, 401, 'login_or_password_is_incorrect');
                    } else {
                        $returnData = msg(1, 200, 'Авторизация успешна', ['user' => $res]);
                    }
                } elseif('create' == $uri[3]) {
                    $errors = [];

                    if (!isset($data->login) || empty(trim($data->login)) ) {
                        $errors['login'] = 'field_is_required';
                    }
                    if (!isset($data->password) || empty(trim($data->password)) ) {
                        $errors['password'] = 'field_is_required';
                    }
                    if (!isset($data->email) || empty(trim($data->email)) ) {
                        $errors['email'] = 'field_is_required';
                    }

                    if (!empty($errors)) {
                        $returnData = msg(0, 422, 'Пожалуйста, заполните все поля', ['errors' => $errors]);
                    } else {
                        $login = trim($data->login);
                        $email = trim($data->email);
                        $password = trim($data->password);
        
                        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            $errors['email'] = 'incorrect_email';
                        }
                        if(strlen($password) < 8) {
                            $errors['password'] = 'password_too_short';
                        }
                        if(strlen($login) < 3) {
                            $errors['login'] = 'login_too_short';
                        }
                        if($user->emailExists($email)) {
                            $errors['email'] = 'email_already_registred';
                        }
                        if($user->loginExists($login)) {
                            $errors['login'] = 'login_already_taken';
                        }

                        if (!empty($errors)) {
                            $returnData = msg(0, 422, 'Пожалуйста, заполните поля корректно', ['errors' => $errors]);
                        } else {
                            $res = $user->create($login, $email, $password);
                            if(false == $res) {
                                $errors['common'] = 'Не удалось создать пользователя';
                                $returnData = msg(0, 422, 'Не удалось создать пользователя', ['errors' => $errors]);
                            } else {
                                $returnData = msg(1, 200, 'Пользователь создан', ['user' => $res]);
                            }
                        }
                    }
                } elseif('list' == $uri[3]) {
                    $res = $user->list();

                    if(false == $res) {
                        $errors['common'] = 'Не удалось получить список пользователей';
                        $returnData = msg(0, 422, 'Не удалось получить список пользователей', ['errors' => $user->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                } else {
                   reject();
                }
            break;

            default:
              reject();
            break;
        }
    break;

    case "tariff":
        if (!checkToken($conn, 1))  {
            $returnData = msg(0, 401, 'Unauthorized');
        } else {
            $tariff = new Tariff($conn);

            switch($requestMethod) {
                case "GET": //Get tariff list
                    $userId = $uri[3];
                    $companyId = $uri[4];
                    $res = $tariff->list($userId, $companyId);

                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось получить список тарифов для пользователя ' . $userId . ' и компании '. $companyId, ['errors' => $tariff->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;
    
                case 'PUT': //Update
                    $tariffId = $uri[3];
                    $res = $tariff->update($tariffId, $data->dateBegin, $data->dateEnd, $data->amount);

                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось сохранить тариф', ['errors' => $tariff->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;
    
                case 'POST': //Create
                    $res = $tariff->create($data->userId, $data->dateBegin, $data->dateEnd, $data->amount, $data->companyId);

                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось создать тариф', ['errors' => $tariff->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;

                default:
                    reject();
                break;
            }
        }
    break;

    case "company":
        if (!checkToken($conn, 1))  {
            $returnData = msg(0, 401, 'Unauthorized');
        } else {
            $company = new Company($conn);

            switch($requestMethod) {
                case "GET": //Get category list
                    $res = $company->list();

                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось получить список компаний', ['errors' => $company->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;
    
                case 'PUT': //Update
                    $companyId = $uri[3];
                    $res = $company->update($companyId, $data->name, $data->inn);

                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось сохранить компанию', ['errors' => $company->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;
    
                case 'POST': //Create
                    $res = $company->create($data->name, $data->inn);

                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось создать компанию', ['errors' => $company->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;

                default:
                    reject();
                break;
            }
        }
    break;

    case "task":
        if (!checkToken($conn, 1))  {
            $returnData = msg(0, 401, 'Unauthorized');
        } else {
            $tariff = new Task($conn);

            switch($requestMethod) {
                case "GET": //Get task list
                    $userId = $uri[3];
    
                    if (isset($uri[4]) && 'find' == $uri[4]) {
                        $term = $uri[5];
                        $res = $tariff->find($userId, urldecode($term) );
                    } else {
                        $res = $tariff->list($userId);
                    }
                     
                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось получить список задач', ['errors' => $tariff->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;
    
                case 'PUT': //Update
                    $taskId = $uri[3];
                    $res = $tariff->update($taskId, $data->name, $data->url, $data->descr);

                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось сохранить задачу', ['errors' => $tariff->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;
    
                case 'POST': //Create
                    $res = $tariff->create($data->userId, $data->task->name, $data->task->url, $data->task->descr, $data->task->company);

                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось создать задачу', ['errors' => $tariff->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;
            }
        }
    break;

    case "entry":
        if (!checkToken($conn, 1))  {
            $returnData = msg(0, 401, 'Unauthorized');
        } else {
            $entry = new Entry($conn);

            switch($requestMethod) {
                case "GET": //Get category list
                    $userId = $uri[3];
                    $date   = $uri[4];
                    $res = $entry->list($userId, $date);
                     
                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось получить список задач', ['errors' => $entry->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;
    
                case 'PUT': //Update
                    $taskId = $uri[3];
                    $res = $entry->update($taskId, $data->begin, $data->end);

                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось сохранить запись', ['errors' => $entry->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;
    
                case 'POST': //Create
                    $res = $entry->create($data->task, $data->begin, $data->end, $data->userId, $data->company);
                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось создать запись', ['errors' => $entry->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;
    
                case 'DELETE': //Delete
                    $id = $uri[3];
                    $res = $entry->delete($id);

                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось создать задачу', ['errors' => $tariff->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;
            }
        }
    break;

    case "report":
        if (!checkToken($conn, 1))  {
            $returnData = msg(0, 401, 'Unauthorized');
        } else {
            $report = new Report($conn);

            switch($requestMethod) {
                case "GET": //Get report
                    $userId    = $uri[3];
                    $dateBegin = $uri[4];
                    $dateEnd   = $uri[5];
                    $res = $report->report($userId, $dateBegin, $dateEnd);
                     
                    if(false == $res) {
                        $returnData = msg(0, 422, 'Не удалось получить отчёт', ['errors' => $report->errors]);
                    } else {
                        $status = 200;
                        $returnData = $res;
                    }
                break;
            }
        }
    break;

    default:
      reject();
    break;
}

http_response_code( is_array($returnData) && isset($returnData['status']) ? $returnData['status'] : $status );
echo json_encode($returnData);

?>
