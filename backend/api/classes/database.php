<?php
// используем для подключения к базе данных MySQL
class Database {

    private $host;
    private $db_name;
    private $username;
    private $password;
    public $conn;
 
    private function init() {
        $dbconfig = parse_ini_file(".env");
        $this->host     = $dbconfig["DB_HOST"];
        $this->db_name  = $dbconfig["DB_NAME"];
        $this->username = $dbconfig["DB_LOGIN"];
        $this->password = $dbconfig["DB_PWD"];

    }

    public function getConnection() {
        $this->init(); 
        $this->conn = null;
        $charset = 'utf8';

        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name . ";charset=" . $charset, $this->username, $this->password);
        } catch(PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->conn;
    }
}
?>