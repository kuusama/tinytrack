<?php

include_once './libs/jwt/JWT.php';

use Firebase\JWT\JWT;

class Company {
    private $conn;
    private $table_name = "company";
    public $errors = [];
 
    public function __construct($db) {
        $this->conn = $db;
    }

    function list() {
        $query = "SELECT id, name, inn FROM " . $this->table_name;
        $stmt = $this->conn->prepare($query);

        if($stmt->execute()) {
            $tArray = [];
            while ($company = $stmt->fetch(PDO::FETCH_OBJ)) {
                $cArray[] = $company;
            }
            return $cArray;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function update($companyId, $name, $inn) {
        $query = "UPDATE " . $this->table_name . " SET name = :name, inn = :inn WHERE id = :id";
        $stmt = $this->conn->prepare($query);

        $companyId = htmlspecialchars(strip_tags($companyId));
        $name = htmlspecialchars(strip_tags($name));
        $inn  = htmlspecialchars(strip_tags($inn));

        $stmt->bindParam(':id', $companyId);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':inn', $inn);

        if(false != $stmt->execute() ) {
            return true;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function create($name, $inn) {
        $query = "INSERT INTO " . $this->table_name . " (name, inn) VALUES(:name, :inn)";
        $stmt = $this->conn->prepare($query);

        $name = htmlspecialchars(strip_tags($name));
        $inn  = htmlspecialchars(strip_tags($inn));

        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':inn', $inn);

        if(false != $stmt->execute() ) {
            return true;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }
}