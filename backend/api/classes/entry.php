<?php

include_once './libs/jwt/JWT.php';
include_once 'task.php';

use Firebase\JWT\JWT;

class Entry {
    private $conn;
    private $table_name = "time";
    public $errors = [];
 
    public function __construct($db) {
        $this->conn = $db;
    }

    private function dailyTotal($userId, $date) {
        $query = "
            SELECT 
                TIME_FORMAT(
                    SEC_TO_TIME( SUM(timestampdiff(second, begin, end)) )
                    , '%H:%i'
                    ) as time, SUM(timestampdiff(second, begin, end)/3600 * tarif.amount) as cost
            FROM " . $this->table_name . "
                INNER JOIN tarif
                    ON tarif.id = time.tarif_id
            WHERE
                user_id = :uid
            AND
                time.begin BETWEEN :beginDate AND :endDate";

        $stmt = $this->conn->prepare($query);
        $userId=htmlspecialchars(strip_tags($userId));
        $date=htmlspecialchars(strip_tags($date));
        $dateStart = $date.' 00:00:00';
        $dateEnd   = $date.' 23:59:59';

        $stmt->bindParam(':uid', $userId);
        $stmt->bindParam(':beginDate', $dateStart, PDO::PARAM_STR);
        $stmt->bindParam(':endDate'  , $dateEnd, PDO::PARAM_STR);

        if($stmt->execute()) {

            return $stmt->fetch(PDO::FETCH_OBJ);
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function list($userId, $date) {
        $query = "
            SELECT 
                time.id, task_id, begin, end, time.tarif_id as tarif, timestampdiff(second, begin, end)/3600 * tarif.amount as cost
            FROM " . $this->table_name . "
                INNER JOIN tarif
                    ON tarif.id = time.tarif_id
            WHERE
                user_id = :uid
            AND
                time.begin BETWEEN :beginDate AND :endDate
            ORDER BY id";

        $stmt = $this->conn->prepare($query);
        $userId=htmlspecialchars(strip_tags($userId));
        $date=htmlspecialchars(strip_tags($date));
        $dateStart = $date.' 00:00:00';
        $dateEnd   = $date.' 23:59:59';

        $stmt->bindParam(':uid', $userId);
        $stmt->bindParam(':beginDate', $dateStart, PDO::PARAM_STR);
        $stmt->bindParam(':endDate'  , $dateEnd, PDO::PARAM_STR);

        if($stmt->execute()) {
            $tArray = [];
            $taskObject = new Task($this->conn);
            while ($record = $stmt->fetch(PDO::FETCH_OBJ)) {
                $object = new stdClass();
                $object->id    = $record->id;
                $object->task  = $taskObject->getOne($record->task_id);
                $object->begin = $record->begin;
                $object->end   = $record->end;
                $object->tarif = $record->tarif;
                $object->cost  = $record->cost;
                $tArray[] = $object;
            }

            $dayObject = new stdClass();
            $dayObject->total = $this->dailyTotal($userId, $date);
            $dayObject->records = $tArray;

            return $dayObject;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function update($id, $begin, $end) {
        $query = "UPDATE " . $this->table_name . " SET begin = :begin, end = :end WHERE id = :id";
        $stmt = $this->conn->prepare($query);

        $id    = htmlspecialchars(strip_tags($id));
        $begin = htmlspecialchars(strip_tags($begin));
        $end   = htmlspecialchars(strip_tags($end));

        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':begin', $begin);
        $stmt->bindParam(':end', $end);

        if(false != $stmt->execute() ) {
            return true;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function create($task, $begin, $end, $userId, $companyId) {
        $query = "INSERT INTO "
            . $this->table_name . "(task_id, tarif_id, begin, end)
            VALUES(:task, (SELECT MAX(id) from tarif where user_id=:userId and company_id=:companyId), :begin, :end)";
        $stmt = $this->conn->prepare($query);

        $userId = htmlspecialchars(strip_tags($userId));
        $task   = htmlspecialchars(strip_tags($task));
        $begin  = htmlspecialchars(strip_tags($begin));
        $end    = htmlspecialchars(strip_tags($end));
        $companyId = htmlspecialchars(strip_tags($companyId));

        $stmt->bindParam(':userId', $userId);
        $stmt->bindParam(':task',   $task);
        $stmt->bindParam(':begin',  $begin);
        $stmt->bindParam(':end',    $end);
        $stmt->bindParam(':companyId', $companyId);

        if(false != $stmt->execute() ) {
            return true;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function delete($id) {
        $query = "DELETE FROM " . $this->table_name . " WHERE id = :id";
        $stmt = $this->conn->prepare($query);

        $id = htmlspecialchars(strip_tags($id));
        $stmt->bindParam(':id', $id);

        if(false != $stmt->execute() ) {
            return true;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }
}