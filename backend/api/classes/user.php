<?php

include_once './libs/jwt/JWT.php';
include_once './libs/jwt/BeforeValidException.php';
include_once './libs/jwt/ExpiredException.php';
include_once './libs/jwt/SignatureInvalidException.php';

use Firebase\JWT\JWT;

class User {
 
    // подключение к БД таблице "users"
    private $conn;
    private $table_name = "user";
    private $key;
    public $errors = [];
 
    // конструктор класса User
    public function __construct($db) {
        $this->conn = $db;

        $config = parse_ini_file(".env");
        $this->key = $config["SECRET_KEY"];
    }

    // Создание нового пользователя
    function create($login, $email, $pwd) {
        $query = "INSERT INTO " . $this->table_name . " SET login = :login, pwd = :pwd, email = :email";
    
        $stmt = $this->conn->prepare($query);
    
        $login=htmlspecialchars(strip_tags($login));
        $pwd=htmlspecialchars(strip_tags($pwd));
        $email=htmlspecialchars(strip_tags($email));
        $password_hash = password_hash($pwd, PASSWORD_BCRYPT);    

        $stmt->bindParam(':login', $login);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':pwd', $password_hash);
    
        if($stmt->execute()) {
            $id = $this->conn->lastInsertId();

            $q2 = "SELECT id, login, email, pwd, level FROM " . $this->table_name . " WHERE id = :id";
            $stmt = $this->conn->prepare($q2);
            $stmt->bindParam(':id', $id);
    
            if($stmt->execute()) {
                $result = $stmt->fetch(PDO::FETCH_OBJ);
                if (false != $result) {
                    if(password_verify($pwd, $result->pwd)) {
                        $object = new stdClass();
                        $object->id = $result->id;
                        $object->login = $result->login;
                        $object->email = $result->email;
                        $object->level = $result->level;
                        return $object;
                    }
                }
            }
        }
    
        return false;
    }

    function update($id, $login, $email, $pwd, $level) {
        $query = "";

        if('' != $pwd) {
            $query = "UPDATE " . $this->table_name . " SET login = :login, pwd = :pwd, email = :email, level = :level WHERE id = :id";            
        } else {
            $query = "UPDATE " . $this->table_name . " SET login = :login, email = :email, level = :level WHERE id = :id";
        }
    
        $stmt = $this->conn->prepare($query);
    
        $login=htmlspecialchars(strip_tags($login));
        $pwd=htmlspecialchars(strip_tags($pwd));
        $email=htmlspecialchars(strip_tags($email));
        $id=htmlspecialchars(strip_tags($id));
        $level=htmlspecialchars(strip_tags($level));

        $stmt->bindParam(':login', $login);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':level', $level);
        $stmt->bindParam(':id', $id);

        if ('' != $pwd) {
            $password_hash = password_hash($pwd, PASSWORD_BCRYPT);
            $stmt->bindParam(':pwd', $password_hash);
        }
    
        if($stmt->execute()) {
            return true;
        }
    
        return false;
    }

    function setToken($id, $token) {
        $query = "UPDATE " . $this->table_name . " SET token = :token WHERE id = :id";
    
        $stmt = $this->conn->prepare($query);
    
        $id=htmlspecialchars(strip_tags($id));
        $stmt->bindParam(':token', $token);
        $stmt->bindParam(':id', $id);
    
        if($stmt->execute()) {
            return true;
        }
    
        return false;
    }

    private function createToken($user) {
        $secretKey  = $this->key; //bin2hex(random_bytes(45));
        $issuedAt   = new DateTimeImmutable();
        $expire     = $issuedAt->modify('+360 minutes')->getTimestamp();      // Add 6 hours
        $serverName = $_SERVER['HTTP_HOST'];
        $username   = $user->login;

        $data = [
            'iat'  => $issuedAt->getTimestamp(),    // Issued at: time when the token was generated
            'iss'  => $serverName,                  // Issuer
            'nbf'  => $issuedAt->getTimestamp(),    // Not before
            'exp'  => $expire,                      // Expire
            'userName' => $username,                // User name
        ];

        $token = JWT::encode($data, $secretKey, 'HS512');

        $this->setToken($user->id, $token);

        return $token;
    }

    function checkToken($token, $level) {
        $result = true;
        $now = new DateTimeImmutable();
        $serverName = $_SERVER['HTTP_HOST'];

        try {
            $decoded = JWT::decode($token, $this->key, ['HS512']);

            if ($decoded->iss !== $serverName ||
                $decoded->nbf > $now->getTimestamp() ||
                $decoded->exp < $now->getTimestamp()) {
                $result = false;
            }

            if (!$this->checkAccess($decoded->userName, $level)) {
                $result = false;
            }
        } catch (\Firebase\JWT\SignatureInvalidException $e) {
            $result = false;
        } catch (\Firebase\JWT\ExpiredException $e) {
            $result = false;
        } catch (\Firebase\JWT\BeforeValidException $e) {
            $result = false;
        }

        return $result;
    }

    function checkAccess($userName, $level) {
        $login = htmlspecialchars(strip_tags($userName));

        $query = "SELECT login, level FROM " . $this->table_name . " WHERE login = :login";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':login', $login);

        if($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_OBJ);

            if (false == $result) {
                return false;
            } else return $result->level >= $level;
        }
        return false;
    }

    function list() {
        $query = "SELECT id, login, email, level FROM " . $this->table_name . " ORDER BY login";
        $stmt = $this->conn->prepare($query);

        if($stmt->execute()) {
            $users = [];
            while ($user = $stmt->fetch(PDO::FETCH_OBJ)) {
                $users[] = $user;
            }
            return $users;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function delete($id) {
        $query = "DELETE FROM " . $this->table_name . " WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $id=htmlspecialchars(strip_tags($id));
        $stmt->bindParam(':id', $id);

        if($stmt->execute()) {
            return true;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function login($login, $pwd) {
        $login = htmlspecialchars(strip_tags($login));
        $pwd = htmlspecialchars(strip_tags($pwd));

        $query = "SELECT id, login, email, pwd, level FROM " . $this->table_name . " WHERE login = :login";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':login', $login);

        if($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_OBJ);
            if (false != $result && $result->level > -1) {
                if(password_verify($pwd, $result->pwd)) {
                    $object = new stdClass();
                    $object->id = $result->id;
                    $object->login = $result->login;
                    $object->email = $result->email;
                    $object->level = $result->level;
                    $object->token = $this->createToken($object);
                    return $object;
                }
            }
        }
    
        return false;
    }

    function loginExists($login) {
        $login = htmlspecialchars(strip_tags($login));

        $query = "SELECT login FROM " . $this->table_name . " WHERE login = :login";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':login', $login);

        if($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_OBJ);
            if (false != $result) {
                return true;
            }
        }
    
        return false;
    }

    function emailExists($email) {
        $email = htmlspecialchars(strip_tags($email));

        $query = "SELECT email FROM " . $this->table_name . " WHERE email = :email";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':email', $email);

        if($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_OBJ);
            if (false != $result) {
                return true;
            }
        }

        return false;
    }
}