<?php

    // Link image type to correct image loader and saver
    // - makes it easier to add additional types later on
    // - makes the function easier to read
    const IMAGE_HANDLERS = [
        IMAGETYPE_JPEG => [
            'load' => 'imagecreatefromjpeg',
            'save' => 'imagejpeg',
            'quality' => 85
        ],
        IMAGETYPE_PNG => [
            'load' => 'imagecreatefrompng',
            'save' => 'imagepng',
            'quality' => 0
        ],
        IMAGETYPE_GIF => [
            'load' => 'imagecreatefromgif',
            'save' => 'imagegif'
        ]
    ];


class File {
    private $uploadDir = SITE_ROOT . '/../files/img/';
    private $thumbDir = SITE_ROOT . '/../files/thumb/';
    private $maxSize = 7340032; // allow max 7 MB
    private $ALLOWED_FILEEXT = array('png', 'jpg', 'jpeg', 'gif', 'svg', 'pdf', 'doc', 'docx' );
    private $IMAGE_FILEEXT = array('png', 'jpg', 'jpeg', 'gif', 'svg');
    private $ALLOWED_MIME = array('image/png', 'image/jpeg', 'image/gif', 'image/svg+xml', 'image/svg', 'application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    private $IMAGE_MIME = array('image/png', 'image/jpeg', 'image/gif');


    private function allowedfile($tempfile, $destpath) {
        $file_ext = pathinfo($destpath, PATHINFO_EXTENSION);
        $file_mime = mime_content_type($tempfile);
        $valid_extn = in_array($file_ext, $this->ALLOWED_FILEEXT);
        $valid_mime = in_array($file_mime, $this->ALLOWED_MIME);
        $allowed_file = $valid_extn && $valid_mime;
        return $allowed_file;
    }

    private function isImage($destpath) {
        $file_ext = pathinfo($destpath, PATHINFO_EXTENSION);
        $file_mime = mime_content_type($destpath);
        $valid_extn = in_array($file_ext, $this->IMAGE_FILEEXT);
        $valid_mime = in_array($file_mime, $this->IMAGE_MIME);
        $allowed_file = $valid_extn && $valid_mime;
        return $allowed_file;
    }

    private function isUploaded($fileArray) {
        $result = true;
        foreach ($fileArray as $tmp_name) {
            if (!is_uploaded_file($tmp_name)) {
                $result = false;
            }
        }
        return $result;
    }
    
    public function handleUpload($itemId) {
        $temp = $_FILES['file']['tmp_name'];

        $filename = $itemId . '_' . date_timestamp_get( date_create() ) . '_' . basename($_FILES['file']['name']);
        $name = basename($_FILES['file']['name']);
        $file_dest = $this->uploadDir . $filename;
        $is_uploaded = is_array($temp) ? $this->isUploaded($temp) : is_uploaded_file($temp);
        $valid_size = $_FILES['file']['size'] <= $this->maxSize && $_FILES['file']['size'] >= 0;

        $success = false;

        if(!$valid_size) {
            $response = 'Файл слишком большой.';
        } else if(!$is_uploaded) {
            $response = "Файл не был загружен.";
        } else if(!$this->allowedfile($temp, $file_dest)) {
            $response = "Тип файла не поддерживается.";
        } else {
            if(move_uploaded_file($temp, $file_dest)) {
                $success = true;
                $response = 'Файл успешно загружен';
                if ($this->isImage($file_dest)) {
                    $this->createThumbnail($file_dest, $this->thumbDir . $filename, 100);
                }
            } else {
                $response = 'Не удалось загрузить файл.';
            }
        }

        $object = new stdClass();
        $object->success = $success;
        $object->response = $response;
        $object->filename = $filename;
        $object->name = $name;
        return $object;
    }

    private function createThumbnail($src, $dest, $targetWidth, $targetHeight = null) {
        $type = exif_imagetype($src);

        // if no valid type or no handler found -> exit
        if (!$type || !IMAGE_HANDLERS[$type]) {
            return null;
        }

        // load the image with the correct loader
        $image = call_user_func(IMAGE_HANDLERS[$type]['load'], $src);

        // no image found at supplied location -> exit
        if (!$image) {
            return null;
        }

        // get original image width and height
        $width = imagesx($image);
        $height = imagesy($image);

        // maintain aspect ratio when no height set
        if ($targetHeight == null) {

            // get width to height ratio
            $ratio = $width / $height;

            // if is portrait
            // use ratio to scale height to fit in square
            if ($width > $height) {
                $targetHeight = floor($targetWidth / $ratio);
            }
            // if is landscape
            // use ratio to scale width to fit in square
            else {
                $targetHeight = $targetWidth;
                $targetWidth = floor($targetWidth * $ratio);
            }
        }

        // create duplicate image based on calculated target size
        $thumbnail = imagecreatetruecolor($targetWidth, $targetHeight);

        // set transparency options for GIFs and PNGs
        if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_PNG) {
            // make image transparent
            imagecolortransparent($thumbnail, imagecolorallocate($thumbnail, 0, 0, 0));

            // additional settings for PNGs
            if ($type == IMAGETYPE_PNG) {
                imagealphablending($thumbnail, false);
                imagesavealpha($thumbnail, true);
            }
        }

        // copy entire source image to duplicate image and resize
        imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);

        // 3. Save the $thumbnail to disk
        // - call the correct save method
        // - set the correct quality level

        // save the duplicate version of the image to disk
        return call_user_func(IMAGE_HANDLERS[$type]['save'], $thumbnail, $dest, IMAGE_HANDLERS[$type]['quality']);
    }

    public function deleteFile($filename) {
        if (false != strpos($filename, '.pdf') ) {
            return unlink($this->uploadDir . $filename);
        }
        return unlink($this->uploadDir . $filename) && unlink($this->thumbDir . $filename);
    }
}
?>
