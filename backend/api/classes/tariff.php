<?php

include_once './libs/jwt/JWT.php';

use Firebase\JWT\JWT;

class Tariff {
    private $conn;
    private $table_name = "tarif";
    public $errors = [];
 
    public function __construct($db) {
        $this->conn = $db;
    }

    function list($userId, $companyId) {
        $query = "SELECT id, date_begin, date_end, amount, company_id FROM " . $this->table_name . " WHERE user_id = :uid AND company_id = :cid ORDER BY id";
        $stmt = $this->conn->prepare($query);
        $userId=htmlspecialchars(strip_tags($userId));
        $companyId=htmlspecialchars(strip_tags($companyId));

        $stmt->bindParam(':uid', $userId);
        $stmt->bindParam(':cid', $companyId);

        if($stmt->execute()) {
            $tArray = [];
            while ($tariff = $stmt->fetch(PDO::FETCH_OBJ)) {
                $tArray[] = $tariff;
            }
            return $tArray;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function update($tariffId, $dateBegin, $dateEnd, $amount) {
        $query = "UPDATE " . $this->table_name . " SET date_begin = :dateBegin, date_end = :dateEnd, amount = :amount WHERE id = :id";
        $stmt = $this->conn->prepare($query);

        $tariffId  = htmlspecialchars(strip_tags($tariffId));
        $dateBegin = htmlspecialchars(strip_tags($dateBegin));
        $dateEnd   = htmlspecialchars(strip_tags($dateEnd));
        $amount    = htmlspecialchars(strip_tags($amount));

        $stmt->bindParam(':id', $tariffId);
        $stmt->bindParam(':dateBegin', $dateBegin);
        $stmt->bindParam(':dateEnd', $dateEnd);
        $stmt->bindParam(':amount', $amount);

        if(false != $stmt->execute() ) {
            return true;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function create($userId, $dateBegin, $dateEnd, $amount, $compayId) {
        $query = "INSERT INTO " . $this->table_name . " (date_begin, date_end, amount, user_id, company_id) VALUES(:dateBegin, :dateEnd, :amount, :userId, :compayId)";
        $stmt = $this->conn->prepare($query);

        $userId  = htmlspecialchars(strip_tags($userId));
        $dateBegin = htmlspecialchars(strip_tags($dateBegin));
        $dateEnd   = htmlspecialchars(strip_tags($dateEnd));
        $amount    = htmlspecialchars(strip_tags($amount));
        $compayId  = htmlspecialchars(strip_tags($compayId));

        $stmt->bindParam(':userId', $userId);
        $stmt->bindParam(':dateBegin', $dateBegin);
        $stmt->bindParam(':dateEnd', $dateEnd);
        $stmt->bindParam(':amount', $amount);
        $stmt->bindParam(':compayId', $compayId);

        if(false != $stmt->execute() ) {
            return true;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }
}