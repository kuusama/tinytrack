<?php

include_once './libs/jwt/JWT.php';

use Firebase\JWT\JWT;

class Task {
    private $conn;
    private $table_name = "task";
    public $errors = [];
 
    public function __construct($db) {
        $this->conn = $db;
    }

    public function getOne($id) {
        $query = "SELECT task.id, task.name as name, url, descr, company_id, company.name as cname FROM " . $this->table_name . "
            INNER JOIN company
                ON task.company_id = company.id
            WHERE task.id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id', $id);

        if($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_OBJ);
            if (false != $result) {
                $object = new stdClass();
                $object->id = $result->id;
                $object->name = $result->name;
                $object->url = $result->url;
                $object->descr = $result->descr;
                $object->cname = $result->cname;
                $object->company = $result->company_id;
                return $object;
            }
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function list($userId) {
        $query = "SELECT task.id, task.name as name, url, descr, company_id as company, company.name as cname FROM " . $this->table_name . "
            INNER JOIN company
                ON task.company_id = company.id
            WHERE user_id = :uid order by id";
        $stmt = $this->conn->prepare($query);
        $userId=htmlspecialchars(strip_tags($userId));
        $stmt->bindParam(':uid', $userId);

        if($stmt->execute()) {
            $tArray = [];
            while ($tariff = $stmt->fetch(PDO::FETCH_OBJ)) {
                $tArray[] = $tariff;
            }
            return $tArray;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function find($userId, $term) {
        $query = "SELECT task.id, task.name as name, url, descr, company_id as company, company.name as cname FROM " . $this->table_name . "
            INNER JOIN company
                ON task.company_id = company.id
            WHERE user_id = :uid AND task.name LIKE CONCAT('%', :term, '%') ORDER BY id";
        $stmt = $this->conn->prepare($query);

        $userId=htmlspecialchars(strip_tags($userId));
        $term=htmlspecialchars(strip_tags($term));

        $stmt->bindParam(':uid', $userId);
        $stmt->bindParam(':term', $term);

        if($stmt->execute()) {
            $tArray = [];
            while ($tariff = $stmt->fetch(PDO::FETCH_OBJ)) {
                $tArray[] = $tariff;
            }
            return $tArray;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function update($id, $name, $url, $descr) {
        $query = "UPDATE " . $this->table_name . " SET name = :name, url = :url, descr = :descr WHERE id = :id";
        $stmt = $this->conn->prepare($query);

        $id    = htmlspecialchars(strip_tags($id));
        $name  = htmlspecialchars(strip_tags($name));
        $url   = htmlspecialchars(strip_tags($url));
        $descr = htmlspecialchars(strip_tags($descr));

        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':url', $url);
        $stmt->bindParam(':descr', $descr);

        if(false != $stmt->execute() ) {
            return true;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    function create($userId, $name, $url, $descr, $companyId) {
        $query = "INSERT INTO " . $this->table_name . " (name, url, descr, user_id, company_id) VALUES(:name, :url, :descr, :userId, :companyId)";
        $stmt = $this->conn->prepare($query);

        $userId  = htmlspecialchars(strip_tags($userId));
        $name = htmlspecialchars(strip_tags($name));
        $url   = htmlspecialchars(strip_tags($url));
        $descr    = htmlspecialchars(strip_tags($descr));
        $companyId  = htmlspecialchars(strip_tags($companyId));

        $stmt->bindParam(':userId', $userId);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':url', $url);
        $stmt->bindParam(':descr', $descr);
        $stmt->bindParam(':companyId', $companyId);

        if(false != $stmt->execute() ) {
            $id = $this->conn->lastInsertId();
            return $this->getOne($id);
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }
}