<?php

include_once './libs/jwt/JWT.php';

use Firebase\JWT\JWT;

class Report {
    private $conn;
    private $table_name = "time";
    public $errors = [];
 
    public function __construct($db) {
        $this->conn = $db;
    }

    function report($userId, $dateBegin, $dateEnd) {
        $userId=htmlspecialchars(strip_tags($userId));
        $dateBegin=htmlspecialchars(strip_tags($dateBegin));
        $dateEnd=htmlspecialchars(strip_tags($dateEnd));

        $dateBegin = $dateBegin.' 00:00:00';
        $dateEnd   = $dateEnd.' 23:59:59';

        $object = new stdClass();

        $totalHours = $this->totalHours($userId, $dateBegin, $dateEnd);
        $timeReport = $this->timeReport($userId, $dateBegin, $dateEnd);
        $taskReport = $this->taskReport($userId, $dateBegin, $dateEnd);
        $companyReport = $this->totalHoursByCompany($userId, $dateBegin, $dateEnd);

        if (false != $totalHours && false != $timeReport && false != $taskReport) {
            $object->totalHours = $totalHours;
            $object->timeReport = $timeReport;
            $object->taskReport = $taskReport;
            $object->companyReport = $companyReport;

            return $object;
        } else {
            return false;
        }
    }

    private function taskReport($userId, $dateBegin, $dateEnd) {
        $query = "SELECT
                    task.id,
                    company.name as company, 
                    task.name,
                    task.url,
                    SUM(timestampdiff(second, begin, end)/3600) as hours,
                    tarif.amount as tarif,
                    SUM(timestampdiff(second, begin, end)/3600) * tarif.amount as cost
                  FROM time
                    INNER JOIN task
                        ON time.task_id = task.id
                    INNER JOIN tarif
                        ON tarif.id = time.tarif_id
                    INNER JOIN company
                        ON task.company_id = company.id
                  WHERE
                    begin BETWEEN :dateBegin AND :dateEnd
                    AND task.user_id = :userId
                  GROUP by task.id, tarif
                  ORDER BY company";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':dateBegin', $dateBegin, PDO::PARAM_STR);
        $stmt->bindParam(':dateEnd'  , $dateEnd,  PDO::PARAM_STR);
        $stmt->bindParam(':userId'   , $userId);
            
        if($stmt->execute()) {
          $tArray = [];
          while ($record = $stmt->fetch(PDO::FETCH_OBJ)) {
              $taskObject = new stdClass();
              $taskObject->company = $record->company;
              $taskObject->task = $record->name;
              $taskObject->url = $record->url;
              $taskObject->hours = $record->hours;
              $taskObject->tarif = $record->tarif;
              $taskObject->cost = $record->cost;
              $taskObject->hourArray = $this->taskHours($userId, $record->id, $dateBegin, $dateEnd);
              $tArray[] = $taskObject;
          }
          return $tArray;
        } else {
          $this->errors = $stmt->errorInfo();
          return false;
        }
    }

    private function timeReport($userId, $dateBegin, $dateEnd) {
        $query = "SELECT DATE(begin) as date, SUM(timestampdiff(second, begin, end)/3600) as hours
                  FROM time
                    INNER JOIN task
                        ON time.task_id = task.id
                  WHERE
	                begin BETWEEN :dateBegin AND :dateEnd
                    AND task.user_id = :userId
                  GROUP BY date
                  ORDER BY date";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':dateBegin', $dateBegin, PDO::PARAM_STR);
        $stmt->bindParam(':dateEnd'  , $dateEnd,  PDO::PARAM_STR);
        $stmt->bindParam(':userId'   , $userId);

        if($stmt->execute()) {
            $tArray = [];
            while ($record = $stmt->fetch(PDO::FETCH_OBJ)) {
                $tArray[] = $record;
            }
            return $tArray;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    private function totalHours($userId, $dateBegin, $dateEnd) {
        $query = "SELECT SUM(hours) as totalhours, SUM(cost) as totalcost FROM
                    (SELECT SUM(timestampdiff(second, begin, end)/3600) as hours,
                            tarif.amount as tarif,
                            SUM(timestampdiff(second, begin, end)/3600) * tarif.amount as cost
                    FROM time
                        INNER JOIN task
                            ON time.task_id = task.id
                        INNER JOIN tarif
                            ON tarif.id = time.tarif_id
                    WHERE
                        begin BETWEEN :dateBegin AND :dateEnd
                        AND task.user_id = :userId
                    GROUP by task.id, tarif
                ) as p";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':dateBegin', $dateBegin, PDO::PARAM_STR);
        $stmt->bindParam(':dateEnd'  , $dateEnd,  PDO::PARAM_STR);
        $stmt->bindParam(':userId'   , $userId);

        if($stmt->execute()) {
            return $stmt->fetch(PDO::FETCH_OBJ);
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    private function totalHoursByCompany($userId, $dateBegin, $dateEnd) {
        $query = "SELECT company, cid, SUM(hours) as totalhours, SUM(cost) as totalcost FROM
                    (SELECT SUM(timestampdiff(second, begin, end)/3600) as hours,
                            tarif.amount as tarif,
                            company.name as company,
                            company.id as cid,
                            SUM(timestampdiff(second, begin, end)/3600) * tarif.amount as cost
                    FROM time
                        INNER JOIN task
                            ON time.task_id = task.id
                        INNER JOIN tarif
                            ON tarif.id = time.tarif_id
                        INNER JOIN company
                            ON company.id = task.company_id
                    WHERE
                        begin BETWEEN :dateBegin AND :dateEnd
                        AND task.user_id = :userId
                    GROUP by company, task.id, tarif
                ) as p GROUP BY company, cid";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':dateBegin', $dateBegin, PDO::PARAM_STR);
        $stmt->bindParam(':dateEnd'  , $dateEnd,  PDO::PARAM_STR);
        $stmt->bindParam(':userId'   , $userId);

        if($stmt->execute()) {
            $companyList = [];
            while ($record = $stmt->fetch(PDO::FETCH_OBJ)) {
                $record->hourReport = $this->timeReportByCompany($userId, $dateBegin, $dateEnd, $record->cid);
                $companyList[] = $record;
            }
            return $companyList;
        } else {
            $this->errors = array_merge($this->errors, $stmt->errorInfo());
            return false;
        }
    }

    private function timeReportByCompany($userId, $dateBegin, $dateEnd, $companyId) {
        $query = "SELECT DATE(begin) as date, SUM(timestampdiff(second, begin, end)/3600) as hours
                  FROM time
                    INNER JOIN task
                        ON time.task_id = task.id
                  WHERE
	                begin BETWEEN :dateBegin AND :dateEnd
                    AND task.user_id = :userId
                    AND task.company_id = :cid
                  GROUP BY date
                  ORDER BY date";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':dateBegin', $dateBegin, PDO::PARAM_STR);
        $stmt->bindParam(':dateEnd'  , $dateEnd,  PDO::PARAM_STR);
        $stmt->bindParam(':userId'   , $userId);
        $stmt->bindParam(':cid'      , $companyId);

        if($stmt->execute()) {
            $tArray = [];
            while ($record = $stmt->fetch(PDO::FETCH_OBJ)) {
                $tArray[] = $record;
            }
            return $tArray;
        } else {
            $this->errors = $stmt->errorInfo();
            return false;
        }
    }

    private function taskHours($userId, $taskId, $dateBegin, $dateEnd) {
        $query = "SELECT SUM(timestampdiff(second, begin, end)/3600) as hours, DATE(begin) as dt
                  FROM time
                    INNER JOIN task
                        ON time.task_id = task.id
                  WHERE
                    begin BETWEEN :dateBegin AND :dateEnd
                    AND task.user_id = :userId
                    AND task.id = :taskId
                  GROUP BY dt
                  ORDER BY dt";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':dateBegin', $dateBegin, PDO::PARAM_STR);
        $stmt->bindParam(':dateEnd'  , $dateEnd,  PDO::PARAM_STR);
        $stmt->bindParam(':userId'   , $userId);
        $stmt->bindParam(':taskId'   , $taskId);
            
        if($stmt->execute()) {
          $hours = "";
          while ($record = $stmt->fetch(PDO::FETCH_OBJ)) {
              $hours .= $record->hours . " + ";
          }
          return $hours;
        } else {
          $this->errors = $stmt->errorInfo();
          return false;
        }
    }
}