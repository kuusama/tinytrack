export enum Language {
    English  = 1,
    Russian  = 2,
    Japanese = 3
}

export class SSettings {
    public userid: number;
    public language: Language;

    constructor(_userid : number) {
        this.userid = _userid;
        this.language = Language.English;
    }
}