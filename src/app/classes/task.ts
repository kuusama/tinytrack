export class Task {
    id: number;
    name: string;
    url: string;
    descr: string;
    action: string = '';
    company: number;
    cname: string = "";

    constructor(id: number, name: string, url: string, descr: string, company: number) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.descr = descr;
        this.company = company;
    }
}