export class Tariff {
    id: number;
    dateBegin: Date;
    dateEnd: Date;
    amount: number;
    number: number;
    company: number;
    action: string = '';

    constructor(id: number, begin: Date, end: Date, amount: number, number: number, company: number) {
        this.id = id;
        this.dateBegin = begin;
        this.dateEnd = end;
        this.amount = amount;
        this.number = number;
        this.company = company;
    }
}