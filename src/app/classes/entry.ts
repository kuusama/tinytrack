import { Task } from "./task";

export class Entry {
    id: number = 0;
    task: Task | null;
    begin: Date;
    end: Date;
    action: string = '';
    uid: number = 0;

    constructor(begin: Date, end: Date, task: Task | null = null) {
        this.task = task;
        this.begin = begin;
        this.end = end;
    }
}