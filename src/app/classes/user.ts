export class User {
    id: number;
    login: string;
    email: string;
    level: number;
    token: string = '';

    constructor(id: number, login: string, email: string, level: number, token: string = '') {
        this.id = id;
        this.login = login;
        this.email = email;
        this.level = level;
        this.token = token;
    }
}
