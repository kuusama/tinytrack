export class Company {
    id: number;
    name: string = "";
    inn: string = '';
    action: string = '';

    constructor(id: number, name: string, inn: string) {
        this.id = id;
        this.name = name;
        this.inn = inn;
    }
}