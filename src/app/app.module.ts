import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ReCaptchaModule } from 'angular2-recaptcha';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field'; 
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon'; 
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDatepickerModule } from '@angular/material/datepicker'; 
import { MatCheckboxModule } from '@angular/material/checkbox'; 
import { OverlayModule } from '@angular/cdk/overlay';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import { TrackerComponent } from './components/tracker/tracker.component';

import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import { LswitcherComponent } from './components/lswitcher/lswitcher.component';
import { GlobalsService } from './services/globals.service';
import { TariffsComponent } from './components/tariffs/tariffs.component';
import { TariffDialogComponent } from './components/tariff-dialog/tariff-dialog.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { TaskDialogComponent } from './components/task-dialog/task-dialog.component';
import { TimeDialogComponent } from './components/time-dialog/time-dialog.component';
import { ReportComponent } from './components/report/report.component';
import { CompanyComponent } from './components/company/company.component';
import { CompanyDialogComponent } from './components/company-dialog/company-dialog.component';
import { UsersComponent } from './components/users/users.component';

// the second parameter 'fr' is optional
registerLocaleData(localeRu, 'ru');

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TrackerComponent,
    LswitcherComponent,
    TariffsComponent,
    TariffDialogComponent,
    TasksComponent,
    TaskDialogComponent,
    TimeDialogComponent,
    ReportComponent,
    CompanyComponent,
    CompanyDialogComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    ReCaptchaModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTabsModule,
    MatListModule,
    MatCardModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatTableModule,
    MatDialogModule,
    MatSelectModule,
    OverlayModule,
    MatIconModule,
    MatSnackBarModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  }),
  ],
  providers: [HttpClientModule, MatDatepickerModule, GlobalsService, {provide: MAT_DATE_LOCALE, useValue: 'ru-RU'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
