import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from 'src/app/classes/user';
import { GlobalsService } from 'src/app/services/globals.service';
import { NetworkService } from 'src/app/services/network.service';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  mUser: User | null = null;

  mCurrentSelectedUser: User | null = null;
  mUserList: Array<User> = [];
  displayedColumns: string[] = ['name'];

  @Input()
  set user(_user: User | null) {
    this.mUser = _user;
  }

  selectUser(user: User) {
    if (this.mCurrentSelectedUser != user) {
      this.mCurrentSelectedUser = user;  
    } else {
      this.mCurrentSelectedUser = null;
    }
  }

  constructor(public dialog: MatDialog, private globals: GlobalsService, private network: NetworkService, private cdr : ChangeDetectorRef) { }

  ngOnInit(): void {
    this.network.getUserList().then( (users) => {
      this.mUserList = users;
    } )    
  }

}
