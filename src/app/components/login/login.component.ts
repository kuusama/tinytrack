import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { ReCaptchaComponent } from 'angular2-recaptcha';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { CookieService } from 'ngx-cookie-service';
import { BroadcastService } from 'src/app/services/broadcast.service';
import { NetworkService } from 'src/app/services/network.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Input()  loginState!: boolean;
  @Output() loginStateChange = new EventEmitter<boolean>();
  @ViewChild(ReCaptchaComponent, {static: false}) captcha!: ReCaptchaComponent;


  ngOnInit() {
    this.doReg = false;
    const cookielist = Cookie.getAll();
    if (null != cookielist) {
      const lang = cookielist.lang;
      const login = cookielist.login;
      const pwd = cookielist.pwd;
      if (null != login && null != pwd) {
        this.login = login;
        this.pwd = pwd;
        this.doLogin();
      }
    }
  }

  public hide : boolean = false;

  login:       string  = '';
  pwd:         string  = '';
  pwd2:        string  = '';
  email:       string  = '';
  doReg:       boolean = false;
  captchaResolved: boolean = false;

  /* Error states */
  emailError: string | null = null;
  passwordError: string | null = null;
  password2Error: string | null = null;
  loginError: string | null = null;

  constructor(private network: NetworkService,
    private broadcast: BroadcastService,
    private cookie: CookieService,
    private translate : TranslateService, private snackBar: MatSnackBar) { }

  public doCheckPassword(): void {
    if (this.pwd == '') {
      this.passwordError = this.translate.instant('password_is_required');
    }
  }
  
  private doCheckPassword2(): void {
    if (this.pwd != this.pwd2 && this.pwd2 == '') {
      this.password2Error = this.translate.instant('password_is_required');
    }

    if (this.pwd != this.pwd2 && this.pwd2 != '') {
      this.password2Error = this.translate.instant('passwords_not_same');
    }
  }

  doCheckEmail(): void {
    if (this.email == '') {
      this.emailError = this.translate.instant('email_is_required');
    }

    if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.email))) {
      this.emailError = this.translate.instant('enter_valid_email');
    }
  }

  doRegister(mode: number = 0) {
    if (0 === mode) {
      this.doReg = true;
    } else {
      this.doReg = false;
    }
  }

  doCancel() {
    this.doReset(1);
    this.doReg = false;
  }

  doSubmit(form: NgForm) {
    if(form.valid) {
      this.network.createUser(this.login, this.email, this.pwd).then( (result) => {
        console.log(result);

        if (result.success) {
          Cookie.set('login', this.login, 10);
          Cookie.set('pwd', this.pwd, 10);
          this.doReg = false;
          this.snackBar.open("Confirmation message was sent successfully. Check e-mail for additional instructions.");
        } else {
        }
      }).catch( (result) => {
        if (result.error.errors.password) {
          this.passwordError = this.translate.instant(result.error.errors.password);
        }

        if (result.error.errors.email) {
          this.emailError = this.translate.instant(result.error.errors.email);
        }

        if (result.error.errors.login) {
          this.loginError = this.translate.instant(result.error.errors.login);
        }

        this.snackBar.open(this.translate.instant('something_wrong'), "OK");
      });
    }
  }

  handleCaptcha(captchaResponse: any) {
    this.captchaResolved = true;
  }

  handleKey(event: any) {
    if (13 == event.keyCode) {
      this.doLogin();
    }
  }

  doLogin() {
    Cookie.set('login', this.login, 10);
    Cookie.set('pwd', this.pwd, 10);

    this.network.login(this.login, this.pwd).then( (result: any) => {
      this.broadcast.userLoggedIn(result.user);
    }).catch( (error: any) => {
      this.loginError = this.translate.instant(error);
    });
  }

  doReset(_loginState: number = 0) {
    this.login = '';
    this.pwd = '';
    this.pwd2 = '';
    this.email = '';
    if (_loginState <= 0) {
      // this.showDialog(_loginState == 0 ? "login_failed" :"not_activated");
    }
    this.captchaResolved = false;
    if (this.captcha) {
      this.captcha.reset();
    }
  }
}
