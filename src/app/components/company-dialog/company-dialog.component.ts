import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Tariff } from 'src/app/classes/tariff';
import { TariffDialogComponent } from '../tariff-dialog/tariff-dialog.component';

@Component({
  selector: 'app-company-dialog',
  templateUrl: './company-dialog.component.html',
  styleUrls: ['./company-dialog.component.scss']
})
export class CompanyDialogComponent {
  action!: string;
  local_data: any;

  doAction() {
    this.dialogRef.close({ event:this.action, data:this.local_data });
  }

  closeDialog() {
    this.dialogRef.close({event:'Cancel'});
  }

  constructor(
    public dialogRef: MatDialogRef<TariffDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Tariff) {
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

}
