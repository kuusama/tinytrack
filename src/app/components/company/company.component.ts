import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Company } from 'src/app/classes/company';
import { User } from 'src/app/classes/user';
import { GlobalsService } from 'src/app/services/globals.service';
import { NetworkService } from 'src/app/services/network.service';
import { CompanyDialogComponent } from '../company-dialog/company-dialog.component';

@Component({
  selector: 'company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  mUser: User | null = null;
  mCompanies: Array<Company> = [];
  mCurrentCompany: Company | null = null;
  
  displayedColumns: string[] = ['name', 'inn'];
  
  @Input()
  set user(_user: User | null) {
    this.mUser = _user;
  }

  selectCompany(company: Company) {
    if (this.mCurrentCompany != company) {
      this.mCurrentCompany = company;  
    } else {
      this.mCurrentCompany = null;
    }
  }

  editCompany(company: Company) {
    this.openDialog('Edit', company);
  }

  addCompany() {
    const company = new Company(0, 'New company', '123456');
    this.openDialog('Create', company);
  }

  openDialog(action: string, obj: Company, $event: any = null) {
    if ($event) {
      $event.stopPropagation();
    }
    obj.action = action;
    const dialogRef = this.dialog.open(CompanyDialogComponent, {
      width: '370px',
      data:obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if(result.event == 'Edit'){
          this.saveCompany(result.data);
        } else if (result.event == 'Create'){
          this.createCompany(result.data);
        }
      }
    });
  }

  saveCompany(cmp: Company) {
    this.network.updateCompany(cmp).then( (result) => {
      this.getCompanies();
    }).catch( (error: any) => {
      this.globals.errorHandler(error);
    });
  }

  createCompany(cmp: Company) {
    this.network.createCompany(cmp).then( (result) => {
      this.getCompanies();
    }).catch( (error: any) => {
      this.globals.errorHandler(error);
    });
  }

  getCompanies() {
    this.network.getCompanyList().then( (result: Array<Company>) => {
      this.mCompanies = result;
    }).catch( (error: any) => {
      this.globals.errorHandler(error);
    });
  }

  constructor(private network: NetworkService, private globals: GlobalsService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getCompanies();
  }

}
