import { ChangeDetectorRef, Component, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Company } from 'src/app/classes/company';
import { Task } from 'src/app/classes/task';
import { GlobalsService } from 'src/app/services/globals.service';
import { NetworkService } from 'src/app/services/network.service';

@Component({
  selector: 'taskdialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.scss']
})
export class TaskDialogComponent {
  action!: string;
  local_data: any;

  mCurrentCompany: Company | null = null;
  mCompanies: Array<Company> = [];

  constructor(
    private network: NetworkService,
    private cdr: ChangeDetectorRef,
    private globals: GlobalsService,
    public dialogRef: MatDialogRef<TaskDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Task) {
    this.local_data = {...data};
    this.action = this.local_data.action;
    this.getCompanies();
  }

  doAction() {
    this.dialogRef.close({ event:this.action, data:this.local_data });
  }

  closeDialog() {
    this.dialogRef.close({event:'Cancel'});
  }

  getCompanies() {
    this.network.getCompanyList().then( (result: Array<Company>) => {
      this.mCompanies = result;
      if ('Edit' == this.action) {
        this.mCurrentCompany = this.mCompanies.filter( e => { return +e.id == +this.local_data.company })[0];
      } else {
        const cid = localStorage.getItem('defaultCompanyId');
        if (null != cid) {
          this.mCurrentCompany = this.mCompanies.filter( e => { return +e.id == +cid })[0];
        } else {
          this.mCurrentCompany = this.mCompanies[0];
        }
        this.local_data.company = this.mCurrentCompany.id;
      }
      
      this.cdr.detectChanges();
    }).catch( (error: any) => {
      this.globals.errorHandler(error);
    });
  }

  onSelectionChange($event: any) {
    this.local_data.company =  $event.value.id;
    localStorage.setItem("defaultCompanyId", $event.value.id);
  }

}
