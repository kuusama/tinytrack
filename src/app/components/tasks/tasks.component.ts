import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Entry } from 'src/app/classes/entry';
import { Task } from 'src/app/classes/task';
import { User } from 'src/app/classes/user';
import { NetworkService } from 'src/app/services/network.service';
import { TimeDialogComponent } from '../time-dialog/time-dialog.component';
import { TaskDialogComponent } from '../task-dialog/task-dialog.component';
import { GlobalsService } from 'src/app/services/globals.service';

@Component({
  selector: 'tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  mUser: User | null = null;

  mDate: Date = new Date();
  mTasks: Array<any> = [];  

  mDayTotal: string = '';
  mDayTotalMoney: number = 0;

  @Input()
  set user(_user: User | null) {
    this.mUser = _user;
  }

  newTime() {
    const date = this.mDate;
    const entry = new Entry(date, new Date(date.getTime() + 1000 * 3600 * 3 ), null);
    entry.uid = this.mUser!.id;
    this.openDialog('Create', entry);
  }

  openDialog(action: string, obj: Entry, $event: any = null) {
    if ($event) {
      $event.stopPropagation();
    }
    obj.action = action;
    const dialogRef = this.dialog.open(TimeDialogComponent, {
      width: '550px',
      data:obj
    });

    dialogRef.afterClosed().subscribe( (result: any) => {
      if (result) {
        if(result.event == 'Edit') {
          this.network.updateEntry(result.data).then( (result) => {
            if(result) {
              this.getDayHistory();
            }
          }).catch( (error) => {
            this.globals.errorHandler(error);
          });
        } else if (result.event == 'Create') {
          this.network.createEntry(this.mUser!.id, result.data).then( (result) => {
            if(result) {
              this.getDayHistory();
            }
          }).catch( (error) => {
            this.globals.errorHandler(error);
          });
        }
      }
    });
  }

  openTaskDialog(action: string, obj: Task, $event: any = null) {
    if ($event) {
      $event.stopPropagation();
    }
    obj.action = action;
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: '550px',
      data:obj
    });

    dialogRef.afterClosed().subscribe( (result: any) => {
      if (result) {
        if(result.event == 'Edit') {
          this.network.updateTask(result.data).then( (result) => {
            if(result) {
              this.getDayHistory();
            }
          }).catch( (error) => {
            this.globals.errorHandler(error);
          });
        }
      }
    });
  }

  getDayHistory() {
    this.network.getDayHistory(this.mUser!.id, this.mDate).then( (result: any) => {
      this.mTasks = [];

      result.records.forEach( (record: any) => {
        const task = new Task(+record.task.id, record.task.name, record.task.url, record.task.descr, record.task.company);
        task.cname = record.task.cname;
        const entry = new Entry(new Date(record.begin), new Date(record.end), task);
        entry.id = +record.id;
        this.mTasks.push(entry);
      });

      this.mDayTotal = result.total.time;
      this.mDayTotalMoney = result.total.cost;

    }).catch( (error: any) => {
      this.globals.errorHandler(error);
    });
  }

  editTime(entry: Entry) {
    this.openDialog('Edit', entry);
  }

  editTask(task: Task) {
    this.openTaskDialog('Edit', task);
  }

  deleteTime(entry: Entry) {
    this.network.deleteEntry(entry).then( (result: boolean) => {
      if (result) {
        this.getDayHistory();
      }
    }).catch( (error: any) => {
      this.globals.errorHandler(error);
    });
  }

  datePrev() {
    this.mDate = new Date(this.mDate.getTime() - 1000 * 3600 * 24);
    this.getDayHistory();
  }

  dateNext() {
    this.mDate = new Date(this.mDate.getTime() + 1000 * 3600 * 24);
    this.getDayHistory();
  }

  constructor(public dialog: MatDialog, private globals: GlobalsService, private network: NetworkService) { }

  ngOnInit(): void {
    this.getDayHistory();
  }

}
