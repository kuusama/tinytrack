import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TariffDialogComponent } from './tariff-dialog.component';

describe('TariffDialogComponent', () => {
  let component: TariffDialogComponent;
  let fixture: ComponentFixture<TariffDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TariffDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TariffDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
