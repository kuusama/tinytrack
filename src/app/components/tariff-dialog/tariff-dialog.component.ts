import { Component, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Tariff } from 'src/app/classes/tariff';

@Component({
  selector: 'tariffdialog',
  templateUrl: './tariff-dialog.component.html',
  styleUrls: ['./tariff-dialog.component.scss']
})
export class TariffDialogComponent {
  action!: string;
  local_data: any;

  constructor(
    public dialogRef: MatDialogRef<TariffDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Tariff) {
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

  doAction() {
    this.dialogRef.close({ event:this.action, data:this.local_data });
  }

  closeDialog() {
    this.dialogRef.close({event:'Cancel'});
  }

}
