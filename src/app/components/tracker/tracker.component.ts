import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/classes/user';
import { BroadcastService } from 'src/app/services/broadcast.service';


@Component({
  selector: 'tracker',
  templateUrl: './tracker.component.html',
  styleUrls: ['./tracker.component.scss']
})
export class TrackerComponent implements OnInit {

  mUser: User | null = null;
  mShowUserMenu: boolean = false;

  @Input()
  set user(_user: User) {
    this.mUser = _user;
  }

  doLogout() {
    this.broadcast.userUnauthorized();
  }

  constructor(private broadcast: BroadcastService) { }

  ngOnInit(): void {
  }

}
