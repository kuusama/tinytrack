import { Time } from '@angular/common';
import { Component, Inject, Optional } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Task } from 'src/app/classes/task';
import { GlobalsService } from 'src/app/services/globals.service';
import { NetworkService } from 'src/app/services/network.service';
import { TaskDialogComponent } from '../task-dialog/task-dialog.component';

@Component({
  selector: 'timedialog',
  templateUrl: './time-dialog.component.html',
  styleUrls: ['./time-dialog.component.scss']
})
export class TimeDialogComponent {
  action!: string;
  local_data: any;

  mTimeBegin: string = "00:00";
  mTimeEnd: string = "00:00";
  mTaskName: string = "";
  mTasks: Array<Task> = [];
  mTypeTimer: any = null;
  mUserId : number = 0;

  private dateToTimeString(date: Date): string {
    let hours = '' + date.getHours(); if (1 == hours.length) { hours = "0" + hours; }
    let mins = '' + date.getMinutes(); if (1 == mins.length) { mins = "0" + mins; }
    return `${hours}:${mins}`;
  }

  private setTimeToDate(date: Date, time: string): Date {
    const hours: number = +time.slice(0, 2);
    const mins: number = + time.slice(3);
    date.setHours(hours, mins, 0, 0);
    return date;
  }

  getTasks() {
    if (null != this.mTypeTimer) {
      clearTimeout(this.mTypeTimer);
      this.mTypeTimer = null;
    }
    this.mTypeTimer = setTimeout( () => {

      if (this.mTaskName.length > 2) {
        this.network.findTask(this.mUserId, this.mTaskName).then( (result) => {
          this.mTasks = result;
        });
      }

    }, 180);
  }

  taskSelected(e: any) {
    this.local_data.task = e.option.value;
    this.mTaskName = e.option.value.name;
  }

  newTask() {
    const newTask = new Task(0, '', '', '', 0);
    this.openDialog('Create', newTask);
  }

  openDialog(action: string, obj: Task, $event: any = null) {
    if ($event) {
      $event.stopPropagation();
    }
    obj.action = action;
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: '550px',
      data:obj
    });

    dialogRef.afterClosed().subscribe( (result: any) => {
      if (result) {
        if (result.event == 'Create') {
          this.network.createTask(this.mUserId, result.data).then( (result: Task) => {
            this.local_data.task = result;
            this.mTaskName = result.name;
          }).catch( (error) => {
            this.globals.errorHandler(error);
          });
        }
      }
    });
  }

  constructor(
    private dialog: MatDialog,
    private network: NetworkService,
    private globals: GlobalsService,
    public dialogRef: MatDialogRef<TimeDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {
    this.local_data = {...data};
    this.action = this.local_data.action;
    this.mTimeBegin = this.dateToTimeString(this.local_data.begin);
    this.mTimeEnd   = this.dateToTimeString(this.local_data.end);
    this.mUserId    = this.local_data.uid;
    if (null != this.local_data.task) {
      this.mTaskName  = this.local_data.task.name;
    }
  }

  doAction() {
    this.local_data.begin = this.setTimeToDate(this.local_data.begin, this.mTimeBegin);
    this.local_data.end   = this.setTimeToDate(this.local_data.end,   this.mTimeEnd);
    this.dialogRef.close({ event:this.action, data:this.local_data });
  }

  closeDialog() {
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit(): void {
  }

}
