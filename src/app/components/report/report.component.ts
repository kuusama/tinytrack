import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/classes/user';
import { NetworkService } from 'src/app/services/network.service';

@Component({
  selector: 'report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  mUser: User | null = null;
  mDate: Date = new Date();
  showTotalHours: boolean = true;
  mReport: any;
  mShowLink: boolean = false;
  mSplitByCompany: boolean = false;

  @Input()
  set user(_user: User | null) {
    this.mUser = _user;
  }

  constructor(private network: NetworkService) { }

  changeLinks($event: any) {
    console.log($event);
  }

  datePrev() {
    this.mDate = new Date(this.mDate.setMonth(this.mDate.getMonth() - 1));
    this.getReport();
  }

  dateNext() {
    this.mDate = new Date(this.mDate.setMonth(this.mDate.getMonth() + 1));
    this.getReport();
  }

  closeDatePicker(date: any, dp?:any) {
    this.mDate = date;
    dp.close();
    this.getReport();
  }

  getReport() {
    this.mReport = {};
    this.network.getMonthReport(this.mUser!.id, this.mDate).then( (result) => {
      this.mReport = result;

      if (this.mReport.timeReport) {
        this.mReport.timeReport.forEach( (day: any) => {
          day.date = new Date(day.date);
        });
      }
    });
  }

  ngOnInit(): void {
    this.getReport();
  }

}
