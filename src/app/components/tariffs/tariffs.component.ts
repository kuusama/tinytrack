import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Company } from 'src/app/classes/company';
import { Tariff } from 'src/app/classes/tariff';
import { User } from 'src/app/classes/user';
import { GlobalsService } from 'src/app/services/globals.service';
import { NetworkService } from 'src/app/services/network.service';
import { TariffDialogComponent } from '../tariff-dialog/tariff-dialog.component';

@Component({
  selector: 'tariffs',
  templateUrl: './tariffs.component.html',
  styleUrls: ['./tariffs.component.scss']
})
export class TariffsComponent implements OnInit {
  mUser: User | null = null;
  mCompany: Company | null = null;
  mTariffs: Array<Tariff> = [];

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  
  @Input()
  set user(_user: User | null) {
    this.mUser = _user;
  }
  
  @Input()
  set company(_company: Company | null) {
    this.mCompany = _company;
    this.getHistory();
  }

  editTarif(tarif: Tariff) {
    this.openDialog('Edit', tarif);
  }

  addTariff() {
    let dt = new Date();
    
    if (this.mTariffs.length > 0) {
      dt = this.mTariffs[this.mTariffs.length - 1].dateEnd;
    } 
    
    const date = dt.getTime();
    const newBeginDate = new Date(date + 1000*3600*24);
    const newEndDate = new Date(date + 1000*3600*24*30*6);
    const amount = this.mTariffs[this.mTariffs.length - 1] ? this.mTariffs[this.mTariffs.length - 1].amount : 0;
    const tariff = new Tariff(0, newBeginDate, newEndDate, amount, 0, this.mCompany!.id);
    this.openDialog('Create', tariff);
  }

  openDialog(action: string, obj: Tariff, $event: any = null) {
    if ($event) {
      $event.stopPropagation();
    }
    obj.action = action;
    const dialogRef = this.dialog.open(TariffDialogComponent, {
      width: '370px',
      data:obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if(result.event == 'Edit'){
          this.saveTariff(result.data);
        } else if (result.event == 'Create'){
          this.createTariff(result.data);
        }
      }
    });
  }

  saveTariff(tariff: Tariff) {
    this.network.updateTariff(tariff).then( (result) => {
      this.getHistory();
    }).catch( (error: any) => {
      this.globals.errorHandler(error);
    });
  }

  createTariff(tariff: Tariff) {
    this.network.createTariff(this.mUser!.id, tariff).then( (result) => {
      this.getHistory();
    }).catch( (error: any) => {
      this.globals.errorHandler(error);
    });
  }

  getHistory() {
    if (this.mUser && this.mCompany) {

      this.network.getTariffHistory(this.mUser.id, this.mCompany.id).then( (result: Array<any>) => {
        this.mTariffs = [];
        
        let i: number = 0;
        result.forEach(e => {
          const tariff = new Tariff(e.id, new Date(e.date_begin), new Date(e.date_end) , e.amount, ++i, e.company_id);
          this.mTariffs.push(tariff);
        });
        
      }).catch( (error: any) => {
        this.mTariffs = [];
        this.globals.errorHandler(error);
      }).finally( () => {
        this.cdr.detectChanges();
      });

    }
  }

  constructor(private network: NetworkService, private globals: GlobalsService, public dialog: MatDialog, private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.getHistory();
  }

}
