import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Language, SSettings } from 'src/app/classes/settings';
import { GlobalsService } from 'src/app/services/globals.service';


@Component({
  selector: 'lswitcher',
  templateUrl: './lswitcher.component.html',
  styleUrls: ['./lswitcher.component.css']
})
export class LswitcherComponent implements OnInit {
  private currentSettings: SSettings = new SSettings(0);
  private languageKeys: Array<any>;
  languages = Language;
  private langLiteral: Array<string> = ['dummy', 'en', 'ru', 'jp'];

  constructor(private globals: GlobalsService, private translate: TranslateService) {
    this.languageKeys  = Object.keys(this.languages).filter(Number);
  }

  ngOnInit() {
    this.currentSettings = this.globals.getCurrentSettings();
    this.changeSelectedLang(this.currentSettings.language);
  }

  private writeToGlobalSettings() {
    this.globals.setCurrentSettings(this.currentSettings);
  }

  changeSelectedLang(lang: Language) {
    this.currentSettings.language = lang;
    this.writeToGlobalSettings();
    this.translate.use(this.langLiteral[lang]);
  }

}
