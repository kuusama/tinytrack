import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BroadcastService } from './broadcast.service';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../classes/user';
import { Tariff } from '../classes/tariff';
import { Task } from '../classes/task';
import { Entry } from '../classes/entry';
import { Company } from '../classes/company';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

const apiPrefix = environment.apiEndpoint;

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  static token: string = "";

  static getOptions() {
    return { headers: new HttpHeaders({ 'Content-Type':  'application/json'}) };
  }

  static getTokenHeaders() : HttpHeaders {
    return new HttpHeaders( {'Authorization': `Bearer ${NetworkService.token}` });
  }

  login(login: string, password: string) {
    const payload = {
      login   : login,
      password: password
    };

    const url = apiPrefix + 'api/user/login';

    return this.http.post<any>(url, JSON.stringify(payload), httpOptions)
    .toPromise();
  }

  createUser(login: string, email: string, password: string) {
    const payload = {
      login   : login,
      email   : email,
      password: password
    };

    const url = apiPrefix + 'api/user/create';

    return this.http.post<any>(url, JSON.stringify(payload), httpOptions)
    .toPromise();
  }

  getUserList(): Promise<Array<User>> {
    const url = apiPrefix + 'api/user/list';
    const options = NetworkService.getOptions();
    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);

    return new Promise<Array<User>>( (resolve, reject) => {
      this.http.post<any>(url, null, options).toPromise().then( (result: Array<any>) => {
        if (result.length > 0) {
          let arr : User[] = [];
          result.forEach(usr => {
            arr.push(new User(usr.id, usr.login, usr.email, usr.level));            
          });
          resolve (arr);
        }
      });
    });
  }

  deleteUser(userId: number): Promise<boolean> {
    const url = `${apiPrefix}api/user/${userId}`;
    const options = NetworkService.getOptions();
    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);

    return new Promise<boolean>( (resolve, reject) => {
      this.http.delete<any>(url, options).toPromise().then( (result: any) => {
        if (typeof result == 'boolean' && result) {
          resolve(result);
        } else {
          reject(result);
        }
      });
    });
  }

  updateUser(user: User, newPwd: string): Promise<boolean> {
    const payload = {
      id: user.id,
      login: user.login,
      email: user.email,
      level: user.level,
      pwd: newPwd
    }

    const url = apiPrefix + 'api/user/update';
    const options = NetworkService.getOptions();

    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.put<any>(url, JSON.stringify(payload), options).toPromise();
  }

  getTariffHistory(userid: number, companyid: number): Promise<Array<any>> {
    const url = `${apiPrefix}api/tariff/${userid}/${companyid}`;
    const options = NetworkService.getOptions();
    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.get<any>(url, options).toPromise();
  }

  updateTariff(tariff: Tariff): Promise<any> {
    const url = apiPrefix + 'api/tariff/' + tariff.id;
    const options = NetworkService.getOptions();

    tariff.dateBegin.setHours(0, 0, 0, 0);
    tariff.dateEnd.setHours(23, 59, 59, 999);

    const payload = {
      dateBegin: new Date(tariff.dateBegin.getTime() - (tariff.dateBegin.getTimezoneOffset() * 60000)).toISOString().slice(0, 19).replace('T', ' '),
      dateEnd: new Date(tariff.dateEnd.getTime() - (tariff.dateEnd.getTimezoneOffset() * 60000)).toISOString().slice(0, 19).replace('T', ' '),
      amount: tariff.amount
    }

    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.put<any>(url, JSON.stringify(payload), options).toPromise();
  }

  createTariff(userId: number, tariff: Tariff): Promise<any> {
    const url = apiPrefix + 'api/tariff';
    const options = NetworkService.getOptions();

    tariff.dateBegin.setHours(0, 0, 0, 0);
    tariff.dateEnd.setHours(23, 59, 59, 999);

    const payload = {
      userId: userId,
      companyId: tariff.company,
      dateBegin: new Date(tariff.dateBegin.getTime() - (tariff.dateBegin.getTimezoneOffset() * 60000)).toISOString().slice(0, 19).replace('T', ' '),
      dateEnd: new Date(tariff.dateEnd.getTime() - (tariff.dateEnd.getTimezoneOffset() * 60000)).toISOString().slice(0, 19).replace('T', ' '),
      amount: tariff.amount
    }

    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.post<any>(url, JSON.stringify(payload), options).toPromise();
  }

  getCompanyList(): Promise<Array<Company>> {
    const url = `${apiPrefix}api/company`;
    const options = NetworkService.getOptions();
    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.get<any>(url, options).toPromise();    
  }

  updateCompany(company: Company): Promise<any> {
    const url = apiPrefix + 'api/company/' + company.id;
    const options = NetworkService.getOptions();

    const payload = {
      name: company.name,
      inn: company.inn
    }

    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.put<any>(url, JSON.stringify(payload), options).toPromise();
  }

  createCompany(company: Company): Promise<any> {
    const url = apiPrefix + 'api/company';
    const options = NetworkService.getOptions();

    const payload = {
      name: company.name,
      inn: company.inn
    }

    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.post<any>(url, JSON.stringify(payload), options).toPromise();
  }

  findTask(userId: number, term: string): Promise<Array<any>> {
    const url = `${apiPrefix}api/task/${userId}/find/${term}`;
    const options = NetworkService.getOptions();
    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.get<any>(url, options).toPromise();
  }

  updateTask(task: Task): Promise<any> {
    const url = apiPrefix + 'api/task/' + task.id;
    const options = NetworkService.getOptions();

    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.put<any>(url, JSON.stringify(task), options).toPromise();
  }

  createTask(userId: number, task: Task): Promise<any> {
    const url = apiPrefix + 'api/task';
    const options = NetworkService.getOptions();

    const payload = {
      userId: userId,
      task: task
    }

    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.post<any>(url, JSON.stringify(payload), options).toPromise();
  }

  updateEntry(entry: Entry): Promise<any> {
    const url = apiPrefix + 'api/entry/' + entry.id;
    const options = NetworkService.getOptions();

    const payload = {
      begin: new Date(entry.begin.getTime() - (entry.begin.getTimezoneOffset() * 60000)).toISOString().slice(0, 19).replace('T', ' '),
      end: new Date(entry.end.getTime() - (entry.end.getTimezoneOffset() * 60000)).toISOString().slice(0, 19).replace('T', ' '),
    }

    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.put<any>(url, JSON.stringify(payload), options).toPromise();
  }

  createEntry(userId: number, entry: Entry): Promise<any> {
    const url = apiPrefix + 'api/entry';
    const options = NetworkService.getOptions();

    console.log(entry.task);

    const payload = {
      userId: userId,
      task: entry.task!.id,
      company: entry.task!.company,
      begin: new Date(entry.begin.getTime() - (entry.begin.getTimezoneOffset() * 60000)).toISOString().slice(0, 19).replace('T', ' '),
      end: new Date(entry.end.getTime() - (entry.end.getTimezoneOffset() * 60000)).toISOString().slice(0, 19).replace('T', ' '),
    }

    console.log("Task");
    console.log(entry.task);

    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.post<any>(url, JSON.stringify(payload), options).toPromise();
  }

  deleteEntry(entry: Entry): Promise<any> {
    const url = apiPrefix + 'api/entry/' + entry.id;
    const options = NetworkService.getOptions();
    
    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.delete<any>(url, options).toPromise();
  }

  getDayHistory(id: number, mDate: Date): Promise<Array<any>> {
    const year = '' + mDate.getFullYear();
    let month  = '' + (mDate.getMonth() + 1); if(1 == month.length) { month = '0' + month; }
    let day    = '' + mDate.getDate();  if(1 == day.length) { day = '0' + day; }
    const url = `${apiPrefix}api/entry/${id}/${year}-${month}-${day}`;
    const options = NetworkService.getOptions();
    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.get<any>(url, options).toPromise();
  }

  getMonthReport(userId: number, date: Date) {
    const beginDate = new Date(date.getFullYear(), date.getMonth(), 1);
    const endDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    const year = '' + beginDate.getFullYear();
    let month  = '' + (beginDate.getMonth() + 1); if(1 == month.length) { month = '0' + month; }
    let day    = '' + beginDate.getDate();  if(1 == day.length) { day = '0' + day; }

    const endYear = '' + endDate.getFullYear();
    let endMonth  = '' + (endDate.getMonth() + 1); if(1 == month.length) { month = '0' + month; }
    let endDay    = '' + endDate.getDate();  if(1 == day.length) { day = '0' + day; }

    const url = `${apiPrefix}api/report/${userId}/${year}-${month}-${day}/${endYear}-${endMonth}-${endDay}`;
    const options = NetworkService.getOptions();
    options.headers = options.headers.append('Authorization', `Bearer ${NetworkService.token}`);
    return this.http.get<any>(url, options).toPromise();
  }

  constructor(private http: HttpClient, private broadcast: BroadcastService, private cookie: CookieService) {
    this.broadcast.$userLoginEvent.subscribe( (user: any) => {
      NetworkService.token = user.token;
      this.cookie.set('user', JSON.stringify(user), 1);
    });

    this.broadcast.$unauthorizedEvent.subscribe( () => {
      NetworkService.token = "";
      this.cookie.deleteAll();
    });
  }
}
