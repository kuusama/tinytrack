import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';
import { SSettings } from '../classes/settings';
import { BroadcastService } from './broadcast.service';

@Injectable()
export class GlobalsService {

  static currentSettings : SSettings;

  constructor(private netService: NetworkService, private broadcast: BroadcastService) {}

  public setCurrentSettings(settings: SSettings) {
    GlobalsService.currentSettings = settings;
    localStorage.setItem('settings', JSON.stringify(settings));
  }

  public getCurrentSettings() {
    if (!GlobalsService.currentSettings) {
      const settings = localStorage.getItem('settings');
      if (!settings) {
        GlobalsService.currentSettings = new SSettings(0);
        localStorage.setItem('settings', JSON.stringify(GlobalsService.currentSettings));
      } else {
        GlobalsService.currentSettings = JSON.parse(settings);
      }
    }
    return GlobalsService.currentSettings;
  }

  public errorHandler(error: any)  {
    console.error(error);
    if (401 == error.status) {
      this.broadcast.userUnauthorized();
    }
  }
}