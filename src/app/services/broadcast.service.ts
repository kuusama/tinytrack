import { Injectable, EventEmitter } from '@angular/core';
import { User } from '../classes/user';

@Injectable({
  providedIn: 'root'
})
export class BroadcastService {

  public $userLoginEvent: EventEmitter<User> = new EventEmitter();
  public $unauthorizedEvent: EventEmitter<null> = new EventEmitter();
  public $refreshPageEvent: EventEmitter<number> = new EventEmitter();
  public $closeAllEvent: EventEmitter<number> = new EventEmitter();

  userLoggedIn(user: User) {
    this.$userLoginEvent.emit(user);
  }

  userUnauthorized() {
    this.$unauthorizedEvent.emit();
  }

  refreshPage(index: any) {
    this.$refreshPageEvent.emit(index);
  }

  closeAll() {
    this.$closeAllEvent.emit();
  }

  constructor() { }
}
