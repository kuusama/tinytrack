import { Component } from '@angular/core';
import { User } from './classes/user';
import { BroadcastService } from './services/broadcast.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'timetracker';
  user: User | null = null;
  

  onTabChange($event: any) {
    this.broadcast.refreshPage($event.index);
  }

  constructor(private broadcast: BroadcastService) {
    broadcast.$userLoginEvent.subscribe( (user) => {
      this.user = user;
    });

    broadcast.$unauthorizedEvent.subscribe( () => {
      this.user = null;
    });
  }
}
